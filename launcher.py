#!/usr/bin/env python3


def parse_cli():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", '--case', default=None, choices=['A', 'B', 'C'],
                        help="If provided it will generate the WordCloud only for one of the Research Areas.")
    parser.add_argument("-r", '--repository', default='phase-3-full-proposal', type=str,
                        help="Path to the phase3 repository. Default: phase-3-full-proposal ")
    parser.add_argument('--full-text', action='store_true', help='Use the full text instead of the simple abstracts')
    parser.add_argument('-s', '--save', action='store_true', help='Save the plot instead of showing it')

    return parser.parse_args()


def main():
    # Get options from cli
    args = parse_cli()

    # Get text corresponding to the case
    from w2wCloud.get_words import get_words
    text = get_words(case=args.case, repository=args.repository, full_text=args.full_text)

    # Generate the image
    from w2wCloud.make_word_cloud import word_cloud_wrapper
    filename = f"word_cloud_{'RA-'+args.case if args.case is not None else 'all'}.png"
    word_cloud_wrapper(text, filename=filename, show=not args.save)


if __name__ == "__main__":
    main()
