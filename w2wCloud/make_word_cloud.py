
import string
from wordcloud import WordCloud, STOPWORDS
from pathlib import Path
import toml

# Get some parameters from the configuration file:
configuration_filename = "configuration.toml"
configuration_path = Path(__file__).resolve().parent.parent / configuration_filename

with configuration_path.open() as f:
    configuration = toml.load(f)

exceptions = configuration["exceptions"]
mask_file = configuration["mask"]
max_words = configuration["max_words"]

width = configuration["width"]
height = configuration["height"]

######################################


def getFrequencyDictForText(sentence) -> dict:
    """
    Given an input text, create a dictionary with all the words and their occurrences.
    :param sentence: str
    :return frequencies : dict
    """
    frequencies = {}

    sentence = sentence.replace("\n", " ")

    for word in sentence.split(" "):
        word = word.translate(str.maketrans('', '', string.punctuation))
        word = word.lower()
        if word in exceptions or word in STOPWORDS:
            continue
        if word in frequencies:
            frequencies[word] += 1
        else:
            frequencies[word] = 1
    return frequencies


def makeImage(frequencies: dict, filename: str, show: bool = False):
    import numpy as np
    from PIL import Image
    import matplotlib.pyplot as plt

    mask = np.array(Image.open(mask_file)) if mask_file else None

    wc = WordCloud(background_color="white", max_words=max_words, mask=mask, random_state=0,
                   width=width, height=height)
    # wc = WordCloud(background_color="white", max_words=1000)
    # generate word cloud
    wc.generate_from_frequencies(frequencies)

    if show:
        # show
        plt.imshow(wc, interpolation="bilinear")
        plt.axis("off")
        plt.show()
    else:
        if filename:
            print(f"Saving word cloud image to {filename!r}.")
            wc.to_image().save(filename)


def word_cloud_wrapper(text: str, filename=None, show=False):
    # Python program to generate WordCloud from a text
    frequencies = getFrequencyDictForText(text)
    makeImage(frequencies, filename=filename, show=show)
