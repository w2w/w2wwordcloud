import os
from pathlib import Path


def find_files(name, path):
    path = Path(path)
    if not path.exists():
        print(f"The folder {path.resolve().as_posix()!r} does not exist.\n"
              f"Most probably you need to download the repository:\n"
              f"git clone https://gitlab.physik.uni-muenchen.de/w2w-proposal/phase-3-full-proposal.git")
        exit(1)
        raise NotADirectoryError(f"Folder {path.resolve()!r} not found.")

    result = []
    for root, dirs, files in os.walk(path):
        for file in files:
            if file == name:
                result.append(os.path.join(root, file))
    return result


def clean_lines(lines):
    lines = (l.strip() for l in lines if l.strip())
    lines = (l for l in lines if l[0] != "%")
    lines = (l for l in lines if l[0] != "\\")
    lines = (l for l in lines if l[0] != "{")
    return lines


def process_proposals(files):
    # Read all files
    text = (open(fp).read() for fp in files)
    merged_text = "".join(text)

    lines = (l for l in merged_text.split("\n"))
    lines = clean_lines(lines)
    lines = list(lines)

    return "\n".join(lines)


def process_abstracts(files):
    # Read all files
    text = (open(fp).read() for fp in files)
    lines = (t.split("\paragraph") for t in text)
    lines = ("\n".join(t[1].split("\n")[1:]) for t in lines)

    lines = clean_lines(lines)
    lines = list(lines)

    return "\n".join(lines)


def get_words(repository: str | Path, case: str | None, full_text: bool = False):
    # Get the corresponding file name
    file_name = "simple-abstract.tex" if not full_text else "project-proposal.tex"

    # Get the list of files
    files = find_files(file_name, repository)

    # Filter by case
    if case is not None:
        assert case in "ABC", "Case sould be A,B, C or None"
        files = [f for f in files if f.count(f"-{case}")]

    assert files, "List of files is empty"

    if full_text:
        return process_proposals(files)
    else:
        return process_abstracts(files)
