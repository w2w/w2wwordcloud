# Waves to Weather Word Cloud generator

Repository to create a word cloud using Waves to Weather proposal data.

To run it, we first need to clone this repository.
```bash
https://gitlab.physik.uni-muenchen.de/w2w/w2wwordcloud
```


The next thing is to download the proposal repository (it requires permissions):

```bash
git clone --depth 1 https://gitlab.physik.uni-muenchen.de/w2w-proposal/phase-3-full-proposal.git
```

Install the dependencies:

> **_NOTE:_**  You might want to install it in a virtual environment! 

```bash
pip install -r requirements.txt
```

After that one can just do:

```bash
./launcher.py 
```

It is possible to use only one of the Research Areas by using the parameter `--case`:
```bash
./launcher.py --case A 
```

Do `./launcher.py -h` to see the all available options.

Also, the following parameters can be modified through the configuration file **configuration.toml**:

```ini
# Figure size
width=1980
height=1200

# Mask filename
mask = "mask.png"

# Additional words that are excluded besides the stopwords (typical words in english)
exceptions = ["will", "project"]

# Maximum number of words
max_words = 500
```

